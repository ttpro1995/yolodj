from django.utils import timezone
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpRequest
from .models import Question
from django.template import loader
from rest_framework.parsers import JSONParser, FormParser, MultiPartParser
from django.views.decorators.csrf import csrf_exempt




# Create your views here.
@csrf_exempt
def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    template = loader.get_template('polls/index.html')
    context = {
        'latest_question_list': latest_question_list,
    }
    return HttpResponse(template.render(context, request))


def detail(request, question_id):
    return HttpResponse("You're looking at question %s." % question_id)


def results(request, question_id):
    response = "You're looking at the results of question %s."
    return HttpResponse(response % question_id)


def vote(request, question_id):
    return HttpResponse("You're voting on question %s." % question_id)

@csrf_exempt
def create_question(request):
    if request.method=="POST":
        request.content_params
        content_type = request.content_type
        if "form" in content_type:
            body = MultiPartParser().parse(request)
            body = body.dict()
            print(body)
        elif "json" in content_type:
            body = JSONParser().parse(request)
        q = Question();
        print(body)
        q.question_text = body["question_text"]
        q.pub_date = timezone.now()
        q.save()

        data = dict()
        data["status"] = "sucess"
        data["id"] = q.id
        return JsonResponse(data)
    else:
        return HttpResponse(status=500)

def simple_upload(request):
    template = loader.get_template('polls/simple_upload_form.html')
    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)
        context = {
            'uploaded_file_url': uploaded_file_url
        }
        return template.render(context, request)
        # return render(request, 'yolo/simple_upload_form.html', {
        #     'uploaded_file_url': uploaded_file_url
        # })
    context = None
    template.render(context, request)
    # return render(request, 'yolo/simple_upload_form.html')
    return template.render(context, request)