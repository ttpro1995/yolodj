from django import forms
from time import time


class UploadFileForm(forms.Form):
    title = forms.CharField(max_length=50)
    file = forms.FileField()


class SimpleUploadFileForm(forms.Form):
    file = forms.FileField()



def handle_uploaded_file(f):
    fname = 'yolo/static/result_image/' + str(time())
    with open(fname+".png", 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
        return fname