from django.urls import path

from . import views

urlpatterns = [
    path('', views.astral_yolo_upload_form, name='index'),
    path('echo', views.echo, name='echo'),
    path('upload_file', views.upload_file, name="upload_file"),
    path('upload_for_image', views.upload_for_image, name="upload_for_image"),
    path('yolo_upload_form', views.yolo_upload_form, name="yolo_upload_form"),
    path('astral_yolo_upload_form', views.astral_yolo_upload_form, name="astral_yolo_upload_form"),
    path('upload_for_image_path', views.upload_for_image_path, name="upload_for_image_path"),
]