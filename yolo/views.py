from django.http import HttpResponse, JsonResponse
from rest_framework.parsers import JSONParser
from django.template import loader
from django.shortcuts import render
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.views.decorators.csrf import csrf_exempt

import cv2

from .forms import UploadFileForm, SimpleUploadFileForm
from .forms import handle_uploaded_file
from .pytorch_yolo_v3 import SingletonYoloWrapper



# Create your views here.

def index(request):
    return HttpResponse("YOLO index here")


def echo(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        return JsonResponse(data)
    elif request.method == "GET":
        data = {"meow":"this is get request"}
        return JsonResponse(data)

@csrf_exempt
def upload_file(requests):
    if requests.method == 'POST':
        form = UploadFileForm(requests.POST, requests.FILES)
        if form.is_valid():
            fname = handle_uploaded_file(requests.FILES['file'])
            print("FNAME " + fname)
            yolo_wrapper = SingletonYoloWrapper.get_instance()
            result, result_img = yolo_wrapper.predict(fname+".png")
            cv2.imwrite(fname+"predict.png", result_img)
            return HttpResponse("success " + fname)
    else:
        form = UploadFileForm()
    return render(requests, 'yolo/upload.html', {'form':form})

@csrf_exempt
def upload_for_image(requests):
    if requests.method == 'POST':
        form = SimpleUploadFileForm(requests.POST, requests.FILES)
        if form.is_valid():
            fname = handle_uploaded_file(requests.FILES['file'])
            print("FNAME " + fname)
            result_fname = fname + ".png"
            try:   # predict
                yolo_wrapper = SingletonYoloWrapper.get_instance()
                result, result_img = yolo_wrapper.predict(fname+".png")
                cv2.imwrite(fname+"predict.png", result_img)
                result_fname = fname+"predict.png"
            except Exception as e:
                result_fname = fname + ".png" # something error, return original pic
                print("something went wrong")
                print(e)
            with open(result_fname, "rb") as f:
                return HttpResponse(f.read(), content_type="image/jpeg")
            # return HttpResponse("success " + fname)
    else:
        form = UploadFileForm()
    return render(requests, 'yolo/uploadv2.html', {'form': form})


@csrf_exempt
def upload_for_image_path(requests):
    """
    Will send the static path of image
    :param requests:
    :return:
    """
    if requests.method == 'POST':
        form = SimpleUploadFileForm(requests.POST, requests.FILES)
        if form.is_valid():
            fname = handle_uploaded_file(requests.FILES['file'])
            print("FNAME " + fname)
            result_fname = fname + ".png"
            try:   # predict
                yolo_wrapper = SingletonYoloWrapper.get_instance()
                result, result_img = yolo_wrapper.predict(fname+".png")
                cv2.imwrite(fname+"predict.png", result_img)
                result_fname = fname+"predict.png"
            except Exception as e:
                result_fname = fname + ".png" # something error, return original pic
                print("something went wrong")
                print(e)

            return HttpResponse(result_fname)
            # return HttpResponse("success " + fname)
    else:
        form = UploadFileForm()
    return render(requests, 'yolo/uploadv2.html', {'form': form})


@csrf_exempt
def yolo_upload_form(requests):
    """
    Only the GET method with html only
    :param requests:
    :return:
    """
    if requests.method == 'GET':
        form = UploadFileForm()
        return render(requests, 'yolo/uploadv2.html', {'form': form})

@csrf_exempt
def astral_yolo_upload_form(requests):
    """

    :param requests:
    :return:
    """
    if requests.method == 'GET':
        form = UploadFileForm()
        return render(requests, 'html5up/html5up-astral/index2.html', {'form': form})
