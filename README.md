This is Django web app that bind YOLO

Requirement

python 3.6

Install lib:

```
conda install pytorch-cpu torchvision-cpu -c pytorch  # GPU is not require. 2s per picture on CPU
or
pip3 install http://download.pytorch.org/whl/cpu/torch-0.4.0-cp36-cp36m-linux_x86_64.whl
pip3 install torchvision
...


pip install opencv-python
pip install Django
pip install djangorestframework
conda install pandas  # or  # pip install pandas
pip install gunicorn
pip install whitenoise
```


Model:

Download yolo weight https://pjreddie.com/media/files/yolov3.weights  and put to yolo/pytorch_yolo_v3.yolov3.weights


Simple usage
```
python manage.py runserver
```

Point browser to http://127.0.0.1:8000/yolo/upload_for_image

